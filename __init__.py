# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import voucher
from . import stock


def register():
    Pool.register(
        voucher.CreateShipment,
        voucher.VoucherHolder,
        voucher.ModifyHolderStart,
        voucher.VoucherMove,
        voucher.Voucher,
        voucher.VoucherPlannedDate,
        voucher.DefinePlannedDateStart,
        stock.Configuration,
        stock.ConfigurationSequence,
        stock.ShipmentInternal,
        stock.ShipmentOut,
        stock.Move,
        module='stock_voucher', type_='model')
    Pool.register(
        voucher.VoucherDone,
        voucher.VoucherModifyHolder,
        voucher.DefinePlannedDate,
        module='stock_voucher', type_='wizard')
    Pool.register(
        voucher.VoucherNote,
        module='stock_voucher', type_='report')
