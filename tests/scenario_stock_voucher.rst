======================
Stock Voucher Scenario
======================

Imports::

    >>> import datetime
    >>> from dateutil.relativedelta import relativedelta
    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.stock_voucher.tests.tools import create_template
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> today = datetime.date.today()
    >>> date = datetime.date(2018, 1, 1)

Install stock_voucher::

    >>> config = activate_modules('stock_voucher')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Get stock locations::

    >>> Location = Model.get('stock.location')
    >>> lost_loc, = Location.find([('type', '=', 'lost_found')])
    >>> storage_loc, = Location.find([('code', '=', 'STO')])
    >>> internal_loc = Location(name='Internal', type='storage')
    >>> internal_loc.save()
    >>> warehouse_loc, = Location.find([('code', '=', 'WH')])
    >>> warehouse_loc2, = warehouse_loc.duplicate()

Create stock configuration::

    >>> Configuration = Model.get('stock.configuration')
    >>> conf = Configuration(1)
    >>> template = create_template(company, 'Product 1')
    >>> product, = template.products
    >>> conf.voucher_product = product
    >>> conf.voucher_comment = '${company_name} ${issued_by_name} ${number} ${reference} ${date} ${product_name}'
    >>> conf.save()

Create parties and addresses::

    >>> Party = Model.get('party.party')
    >>> customer = Party(name='Customer')
    >>> customer.save()
    >>> emitter = Party(name='Emitter')
    >>> emitter.save()
    >>> issued_by = Party(name='Issuer')
    >>> issued_by.save()
    >>> carrier = Party(name='Receiver')
    >>> carrier.save()
    >>> address = carrier.addresses[0]
    >>> address.name = 'Address Receiver'
    >>> address.save()
    >>> lost_loc.address = address
    >>> lost_loc.save()
    >>> warehouse_loc.service_party = issued_by
    >>> warehouse_loc.save()

Create carrier::

    >>> Carrier = Model.get('carrier')
    >>> carrier = Carrier(party=carrier)
    >>> carrier_product, = create_template(company, 'Carrier service', 'service').products
    >>> carrier.carrier_product = carrier_product
    >>> carrier.save()

Create voucher::

    >>> Shipment = Model.get('stock.shipment.internal')
    >>> Voucher = Model.get('stock.voucher')
    >>> voucher = Voucher()
    >>> voucher.company = company
    >>> voucher.issued_by = issued_by
    >>> voucher.carrier = carrier
    >>> voucher.date = date
    >>> voucher.reference = 'R1'
    >>> voucher.quantity = 10
    >>> voucher.issuing_address.rec_name
    'Issuer'
    >>> voucher.emitter = emitter
    >>> voucher.issuing_warehouse = warehouse_loc
    >>> voucher.warehouse = warehouse_loc2
    >>> voucher.save()
    >>> voucher.number
    '1'
    >>> voucher.quantity
    10.0
    >>> voucher.product == conf.voucher_product
    True
    >>> voucher.state
    'draft'
    >>> voucher.click('wait')
    >>> shipments = Shipment.find([])
    >>> len(shipments)
    0
    >>> internal_loc.address = voucher.issuing_address
    >>> internal_loc.save()
    >>> voucher.click('process')

Check values after processing::

    >>> voucher.state
    'processing'
    >>> len(voucher.receiver_moves)
    1
    >>> shipment = voucher.receiver_moves[0].shipment
    >>> len(shipment.moves)
    1
    >>> move, = voucher.receiver_moves
    >>> move.quantity
    10.0
    >>> move.product == voucher.product
    True
    >>> shipment.voucher
    True
    >>> move.voucher
    True
    >>> shipment.from_location == carrier.party.supplier_location
    True
    >>> shipment.to_location == internal_loc
    True
    >>> voucher_shipments = Shipment.find([('voucher', '=', False)])
    >>> len(voucher_shipments)
    0
    >>> voucher_shipments = Shipment.find([('voucher', '!=', True)])
    >>> len(voucher_shipments)
    0
    >>> voucher_shipments = Shipment.find([('voucher', '=', True)])
    >>> len(voucher_shipments)
    1
    >>> voucher_shipments = Shipment.find([('voucher', '!=', False)])
    >>> len(voucher_shipments)
    1
    >>> shipment.comment
    ' Issuer 1 R1 2018-01-01 Product 1'

Create a second voucher::

    >>> voucher2 = Voucher()
    >>> voucher2.company = company
    >>> voucher2.issued_by = issued_by
    >>> voucher2.carrier = carrier
    >>> voucher2.date = today - relativedelta(days=3)
    >>> voucher2.reference = 'R2'
    >>> voucher2.quantity = 8
    >>> voucher2.emitter = emitter
    >>> voucher2.click('wait')
    >>> voucher2.click('process')

Done 2 vouchers together::

    >>> done_voucher = Wizard('stock.voucher.done', [voucher, voucher2])
    >>> done_voucher.form.received_by = carrier.party
    >>> done_voucher.form.received_date = today
    >>> done_voucher.form.to_location = storage_loc
    >>> done_voucher.execute('shipment')
    >>> move, = done_voucher.form.moves
    >>> move.quantity = 5
    >>> done_voucher.execute('done') # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
        ...
    UserError: The sum of moves quantity ("5.0") must match the sum of vouchers quantity ("20.0").
    >>> move.quantity = 18
    >>> done_voucher.execute('done')
    >>> voucher.state
    'done'
    >>> move, = voucher.moves
    >>> move.quantity
    18.0
    >>> len(move.vouchers)
    2

Undo vouchers::

    >>> voucher.click('undo')
    >>> shipment, = voucher.shipments_internal
    >>> shipment.state
    'cancel'
    >>> voucher.state
    'processing'
    >>> voucher2.reload()
    >>> voucher2.state
    'processing'

Unprocess first voucher::

    >>> voucher.click('unprocess')
    >>> voucher.receiver_moves[0].state
    'cancel'
    >>> voucher.receiver_moves[0].shipment.state
    'cancel'
    >>> voucher.state
    'waiting'

Process and do modifing shipment moves::

    >>> voucher.click('process')
    >>> product2, = create_template(company, 'Product 2').products
    >>> done_voucher = Wizard('stock.voucher.done', [voucher, voucher2])
    >>> done_voucher.form.received_by = carrier.party
    >>> done_voucher.form.received_date = today
    >>> done_voucher.form.shipment_type = 'shipment_out'
    >>> done_voucher.form.customer = customer
    >>> done_voucher.form.warehouse = storage_loc.warehouse
    >>> done_voucher.execute('shipment')
    >>> move1 = done_voucher.form.outgoing_moves[0]
    >>> move1.to_location = customer.customer_location
    >>> move1.quantity = 15
    >>> move2 = done_voucher.form.outgoing_moves.new()
    >>> move2.from_location = move1.from_location
    >>> move2.to_location = customer.customer_location
    >>> move2.product = product2
    >>> move2.quantity = 3
    >>> done_voucher.execute('done')
    >>> voucher.reload()
    >>> voucher.state
    'done'
    >>> len(voucher.shipments_out)
    1
    >>> len([m for m in voucher.moves if m.state != 'cancel'])
    1
    >>> voucher2.reload()
    >>> len([m for m in voucher2.moves if m.state != 'cancel'])
    2

Modify holder when waiting::

    >>> len(voucher.holders)
    0
    >>> voucher.warehouse = storage_loc.warehouse
    >>> voucher.click('wait')
    >>> Wizard('stock.voucher.holder.modify_holder', [voucher, voucher2])